﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryManagment : MonoBehaviour {
    Animator anim;
    PlayerController player;

    [Header("On Equip Items")]
    private bool inventoryActive;
    
    public int weapon1Damage;

    public int weapon2Damage;

    public int armor1maxHpIncreasment;

    public int armor2maxHpIncreasment;

    public int helmHpIncreaseStats;
    public int bootsSpeedIncreaseStats;

    private bool armor1Equiped;
    private bool armor2Equiped;
    private bool healarmor1, healarmor2;
    private bool helmEquiped;
    private int playerMaxHealtInitial;
    [Header("Drag")]
    [SerializeField]GameObject[] itemsOnStats;
    [SerializeField] GameObject[] selectedItems;
    // Use this for initialization
    void Start () {
        
        inventoryActive = false;
        anim = GetComponent<Animator>();
        player = FindObjectOfType<PlayerController>();
        playerMaxHealtInitial = player.GetPlayerMaxHealth();
    }
	
	// Update is called once per frame
	void Update () {
        OnPressI();

    }

    void OnPressI()
    {
        if (Input.GetKeyDown(KeyCode.I)/* || Input.GetKeyDown("joystick button 0")*/)
        {
            if (!inventoryActive)
            {
                anim.SetTrigger("GoUpInventory");
                //itemsManager.SetActive(true);
                inventoryActive = true;
            }
            else if (inventoryActive)
            {
                anim.SetTrigger("GoDownInventory");
                // itemsManager.SetActive(false);
                inventoryActive = false;
            }
        }
    }
    public void Button1()//weapon1
   {
        if (!itemsOnStats[0].activeInHierarchy)
        {

            selectedItems[0].SetActive(true);
            itemsOnStats[0].SetActive(true);
            player.SetPlayerAttackDmg(weapon1Damage);
            if (itemsOnStats[1].activeInHierarchy)
            {
                itemsOnStats[1].SetActive(false);
                selectedItems[1].SetActive(false);
            }
        }
   }
    public void Button2()//weapon2
    {
        if (!itemsOnStats[1].activeInHierarchy)
        {
            selectedItems[1].SetActive(true);
            itemsOnStats[1].SetActive(true);
            player.SetPlayerAttackDmg(weapon2Damage);
            if (itemsOnStats[0].activeInHierarchy)
            {
                itemsOnStats[0].SetActive(false);
                selectedItems[0].SetActive(false);
            }
        }
    }
    public void Button3()//armor1
    {
        if (!itemsOnStats[2].activeInHierarchy)
        {
            selectedItems[2].SetActive(true);
            itemsOnStats[2].SetActive(true);
            if (helmEquiped)
            {
                player.SetPlayerMaxHealth(armor1maxHpIncreasment + helmHpIncreaseStats);
            }
            else
            {
                player.SetPlayerMaxHealth(armor1maxHpIncreasment);
            }
            if (!healarmor1)
            {
                player.SetPlayerHealth(player.GetPlayerHealth() + armor1maxHpIncreasment);
                healarmor1 = true;
            }
                
            
            if (itemsOnStats[3].activeInHierarchy)
            {
                itemsOnStats[3].SetActive(false);
                selectedItems[3].SetActive(false);
            }
            armor1Equiped = true;
            armor2Equiped = false;
        }
    }
    public void Button4()//armor2
    {
        if (!itemsOnStats[3].activeInHierarchy)
        {
            selectedItems[3].SetActive(true);
            itemsOnStats[3].SetActive(true);

            if (helmEquiped)
            {
                player.SetPlayerMaxHealth(armor2maxHpIncreasment + helmHpIncreaseStats);
            }
            else
            {
                player.SetPlayerMaxHealth(armor2maxHpIncreasment);
            }
            if (!healarmor2)
            {
                player.SetPlayerHealth(player.GetPlayerHealth() + armor2maxHpIncreasment);
                healarmor2 = true;
            }
            if (itemsOnStats[2].activeInHierarchy)
            {
                itemsOnStats[2].SetActive(false);
                selectedItems[2].SetActive(false);
            }
            armor1Equiped = false;
            armor2Equiped = true;
        }
    }
    public void Button5()//helm
    {
        if (!itemsOnStats[4].activeInHierarchy)
        {
            if (armor1Equiped)
            {
                player.SetPlayerMaxHealth( helmHpIncreaseStats +armor1maxHpIncreasment);
                if (!helmEquiped)
                {
                    player.SetPlayerHealth(player.GetPlayerHealth() + helmHpIncreaseStats);
                    helmEquiped = true;
                }   
            }
            if (armor2Equiped)
            {
                player.SetPlayerMaxHealth(helmHpIncreaseStats + armor2maxHpIncreasment);
                if (!helmEquiped)
                {
                    player.SetPlayerHealth(player.GetPlayerHealth() + helmHpIncreaseStats);
                    helmEquiped = true;
                }
            }
            itemsOnStats[4].SetActive(true);
            selectedItems[4].SetActive(true);
            player.SetPlayerAttackDmg(weapon1Damage);
        }
    }
    public void Button6()//boots
    {
        if (!itemsOnStats[5].activeInHierarchy)
        {
            
            player.SetPlayerSpeed(player.GetPlayerSpeed() + bootsSpeedIncreaseStats);
            itemsOnStats[5].SetActive(true);
            selectedItems[5].SetActive(true);
            player.SetPlayerAttackDmg(weapon1Damage);
        }
        
    }


}
