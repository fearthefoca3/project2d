﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItems : MonoBehaviour {

    [SerializeField]GameObject buttonOne;

    [SerializeField] GameObject buttonTwo;

    [SerializeField] GameObject buttonThree;

    [SerializeField] GameObject buttonFour;

    [SerializeField] GameObject buttonFive;

    [SerializeField] GameObject buttonSix;
    // Use this for initialization
   

    public void SetActiveWeapon1()
    {
        buttonOne.gameObject.SetActive(true);
    }
    public void SetActiveWeapon2()
    {
        buttonTwo.gameObject.SetActive(true);
    }
    public void SetActiveWeapon3()
    {
        buttonThree.gameObject.SetActive(true);
    }
    public void SetActiveWeapon4()
    {
        buttonFour.gameObject.SetActive(true);
    }
    public void SetActiveWeapon5()
    {
        buttonFive.gameObject.SetActive(true);
    }
    public void SetActiveWeapon6()
    {
        buttonSix.gameObject.SetActive(true);
    }
}
