﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManagment : MonoBehaviour
{

    public bool GameIsPaused;
    public GameObject CanvasPause;
    public AudioSource PauseMusic;
    public AudioSource BgMusic;
    public AudioSource pj;

    // Use this for initialization
    void Awake()
    {
        GameIsPaused = false;
        CanvasPause.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!GameIsPaused)
            {
                Pause();
            }
            else
            {
                Resume();
            }
        }
        

    }
    public void Pause()
    {
        GameIsPaused = true;
        Time.timeScale = 0;
        CanvasPause.SetActive(true);
        PauseMusic.Play();
        BgMusic.Pause();
        pj.Pause();

    }
    public void Resume()
    {
        Time.timeScale = 1;
        CanvasPause.SetActive(false);
        GameIsPaused = false;
        PauseMusic.Stop();
        BgMusic.UnPause();
        pj.UnPause();


    }
    

    public void QuitGame()
    {

        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();

    }

}
