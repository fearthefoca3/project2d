﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {

    [SerializeField] PlayerController player;
    void Start () {
        player = FindObjectOfType<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.GetComponent<PlayerController>().MoveObjects())
            {
                transform.parent = other.transform;
                transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                transform.parent = null;

                transform.GetChild(1).gameObject.SetActive(true);
            }
        }
    }
}
