﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherController : MonoBehaviour {

    // Use this for initialization
    [SerializeField] PlayerController player;
   public bool pushing;
    public bool top;
    public bool down;
    public bool left;
    public bool right;


    void Start () {
        player = FindObjectOfType<PlayerController>();
        pushing = false;
    }

    // Update is called once per frame
    void Update () {

        if (pushing)
        {
            player.GetComponent<PlayerController>().isPushing = true;

            if (down)
            {

                player.rb2D.AddForce(new Vector2(0, -500*Time.deltaTime));

            }
            if (top)
            {
                player.rb2D.AddForce(new Vector2(0, 500 * Time.deltaTime));

            }
            if (left)
            {
                player.rb2D.AddForce(new Vector2(-500 * Time.deltaTime, 0));

            }
            if (right)
            {
                player.rb2D.AddForce(new Vector2(500 * Time.deltaTime, 0));


            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            pushing = true;
            if (other.GetComponent<PlayerController>().pusher != null)
            {
                other.GetComponent<PlayerController>().pusher.pushing = false;
            }
            other.GetComponent<PlayerController>().pusher = this;

        }
    }


}
