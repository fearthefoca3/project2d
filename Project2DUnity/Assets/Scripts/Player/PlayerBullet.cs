﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class PlayerBullet : MonoBehaviour
{

    public float speed = 4f;
    public int bulletDmg = 5;
    Vector2 moveDirection;

    public int DirectionShooting;
    //public GameObject explosion;


    private Rigidbody2D rb2D;

    // Use this for initialization
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        if(DirectionShooting == 0)
        {
            moveDirection = (Vector2.up).normalized * speed;
        }
        else if(DirectionShooting == 1)
        {
            moveDirection = (Vector2.down).normalized * speed;
        }
        else if (DirectionShooting == 2)
        {
            moveDirection = (Vector2.right).normalized * speed;
        }
        else if (DirectionShooting == 3)
        {
            moveDirection = (Vector2.left).normalized * speed;
        }
        //moveDirection = (player.transform.position - transform.position).normalized * speed;
        rb2D.velocity = new Vector2(moveDirection.x, moveDirection.y);
    }


    //private void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        player.SetPlayerHealth(player.GetPlayerHealth() - bulletDmg);
    //        Destroy(gameObject);
    //    }
    //}

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.collider.CompareTag("Bullet"))
        {

            Destroy(gameObject);
        }
        else if (other.collider.CompareTag("Bullet"))
        {

            Destroy(other.gameObject);
        }
    }
    


}
