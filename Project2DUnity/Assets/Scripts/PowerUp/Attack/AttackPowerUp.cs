﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPowerUp : MonoBehaviour {
    PlayerController player;
    public int increaseAttack;
    public float timerAttackIncreased;
    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            player.IncreaseAttack(timerAttackIncreased, increaseAttack);
            Destroy(gameObject);
        }

    }

   
}
