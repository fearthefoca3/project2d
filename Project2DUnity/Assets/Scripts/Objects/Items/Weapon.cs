﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Weapon : MonoBehaviour {
    //PlayerController player;
    public int objectNumber;
    PlayerController player;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>();
      
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Button.SetActive(true);
            switch (objectNumber)
            {
                case 1:
                    GameObject.Find("Items").transform.GetChild(1).gameObject.SetActive(true);

                    break;
                case 2:
                    //GameObject.Find("Weapon2Button").SetActive(true);
                    GameObject.Find("Items").transform.GetChild(2).gameObject.SetActive(true);
                    break;
                case 3:
                   // GameObject.Find("Armor1Button").SetActive(true);
                    GameObject.Find("Items").transform.GetChild(3).gameObject.SetActive(true);
                    break;
                case 4:
                    //GameObject.Find("Armor2Button").SetActive(true);
                    GameObject.Find("Items").transform.GetChild(4).gameObject.SetActive(true);
                    break;
                case 5:
                    //GameObject.Find("Helm").SetActive(true);
                    GameObject.Find("Items").transform.GetChild(5).gameObject.SetActive(true);
                    break;
                case 6:
                    //GameObject.Find("Boots").SetActive(true);
                    GameObject.Find("Items").transform.GetChild(6).gameObject.SetActive(true);
                    break;
                case 7:
                    GameObject.Find("Items").transform.GetChild(7).gameObject.SetActive(true);
                    GameObject.Find("PlayerItems").transform.GetChild(6).gameObject.SetActive(true);
                    player.SetHasKeyLvl2(true);
                    break;
                default:
                    break;
            }
            Destroy(gameObject);
        }
    }
}
