﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpPowerUp : BasicPowerUp {

    // Use this for initialization
    PlayerController player;
    [Range(0,100)] public int percentageOfHealing;
    public int healingAmount;
    void Start () {
        player = FindObjectOfType<PlayerController>();
        healingAmount = player.maxHp * percentageOfHealing;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
   
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.GetPlayerHealth() < player.maxHp - healingAmount)
            {

                player.SetPlayerHealth(player.GetPlayerHealth() + healingAmount);
            }
            else
            {
                player.SetPlayerHealth(player.maxHp);
            }
        }
        Destroy(this.gameObject);
    }
}
