﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {


    [SerializeField] PlayerController player;

    public Image ImgHealthBar;
    public Image ImgRedHealthBar;
    public Text TxtHealth;
    public float RedBarTimer;
    public int Min;
    public float HpTimer;
    public int Max;

    private int mCurrentValue;

    private float mCurrentPercent;
	// Use this for initialization
	void Start () {
        RedBarTimer = 0.5f;
        player = FindObjectOfType<PlayerController>();
        InvokeRepeating("CheckPlayerHealth", 0.3f, 0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        HandleBar();
    }
    public void HandleBar()
    {
        if (ImgRedHealthBar.fillAmount != ImgHealthBar.fillAmount)
        {
            ImgRedHealthBar.fillAmount = Mathf.Lerp(ImgRedHealthBar.fillAmount, ImgHealthBar.fillAmount, HpTimer * Time.deltaTime);
        }
    }
    public void SetHealth(int healt)
    {
        if (healt != mCurrentValue)
        {
            if (Max - Min == 0)
            {
                mCurrentValue = 0;
                mCurrentPercent = 0;
            }
            else
            {
                mCurrentValue = healt;
                mCurrentPercent = (float)mCurrentValue / (float)(Max - Min);
            }

            TxtHealth.text = string.Format(Mathf.RoundToInt(mCurrentPercent * 100) + " %");
            ImgHealthBar.fillAmount = mCurrentPercent;

            StartCoroutine("HealthBarController");

        }
    }

    public float CurrentPercent
    {
        get { return mCurrentValue; }
    }

    public void CheckPlayerHealth()
    {
        if(!player.death)
              SetHealth(player.GetComponent<PlayerController>().GetPlayerHealth());
    }
    IEnumerator HealthBarController()
    {

        yield return new WaitForSeconds(RedBarTimer);

    }
}
