﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsController : MonoBehaviour {

    [Header("Vars to drag")]
    public GameObject doorClossed;
    public GameObject doorOpened;
    public bool inside;

    public void Start()
    {
        inside = true;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (inside)
        {
            if (other.CompareTag("Player"))
            {
            
                if (doorClossed.activeInHierarchy)
                {
                    doorClossed.SetActive(false);
                    doorOpened.SetActive(true);
                }
            }
            
        }
      

    }
    public void CloseDoor()
    {
        doorClossed.SetActive(true);
        doorOpened.SetActive(false);
    }
}
