﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class BombBoss : MonoBehaviour {

    PlayerController player;
    public float speed;
    public int bombDmg;
    private Rigidbody2D rb2D;
    public Animator anim;
    Vector2 moveDirection;
    public float timeToDie = 5;
    public bool cancel = false;
    // Use this for initialization
    void Start () {

        rb2D = GetComponent<Rigidbody2D>();
        player = GameObject.FindObjectOfType<PlayerController>();
        StartCoroutine("DestroyAfterTime");
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if(cancel == false)
        {

            moveDirection = (player.transform.position - transform.position).normalized * speed;
            rb2D.velocity = new Vector2(moveDirection.x, moveDirection.y);
        }
        else
        {
            rb2D.velocity = new Vector2(0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            anim.SetTrigger("Explosion");
            CancelMovment();
            player.SetPlayerHealth(player.GetPlayerHealth()- bombDmg);
            //Destroy(gameObject);
        }
    }
    private void DestroyObject()
    {
        Destroy(gameObject);
    }
    private void CancelMovment()
    {
        cancel = true;
    }
    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(timeToDie);

        anim.SetTrigger("Explosion");
        CancelMovment();
        //DestroyObject();
    }
    

}
