﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour
{
    [Header("Vars to debug and cotrol")]
    Vector3 startPosition;
    [SerializeField]private float initialDuration;
    private const int endDuration = 0;

    [Header("Vars to define")]
    [Header("Shake")]
    [Range(0, 100)] public float powerModifier = 0.0f;
    private float power = 0.7f;
    public float duration = 1.0f;
    private const float maxPercentage = 100.0f;
    public float slowDownAmount = 1.0f;
    public bool shouldShake = false;

    [Header("Vars to drag")]
    public Transform target;
    public Transform mainCamera;
    
    // Use this for initialization
    void Start()
    {
        mainCamera = Camera.main.transform;
        startPosition = mainCamera.localPosition;
        initialDuration = duration;
    }

    // Update is called once per frame
    void Update()
    {
        startPosition = mainCamera.localPosition;
        power = powerModifier / maxPercentage;
        if (shouldShake)
        {
            if (duration > endDuration)
            {
                mainCamera.localPosition = startPosition + Random.insideUnitSphere * power;
                duration -= Time.deltaTime * slowDownAmount;
            }
            else
            {
                shouldShake = false;
                duration = initialDuration;
                mainCamera.localPosition = startPosition;
            }
        }
    }

   
}
