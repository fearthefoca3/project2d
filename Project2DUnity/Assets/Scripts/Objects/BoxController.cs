﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {
    [Header("Vars to debug and cotrol")]
    [SerializeField]Animator anim;
    [SerializeField]PlayerController player;
    [SerializeField] private float animSpeed = 0.5f;
    private float stopAnimSpeed = 0.0f;
    [SerializeField]private bool notUsed;
    public GameObject objeto;
    // Use this for initialization
    private void Awake()
    {
        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
        }
                

        transform.GetChild(0).gameObject.SetActive(false);
    }
    void Start () {

        anim = gameObject.GetComponent<Animator>(); // find the current instance of the player script:
        player = FindObjectOfType<PlayerController>();
        anim.speed = 0.0f;
        notUsed = true;
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //InteractWithObjects()
            if (notUsed)
            {
                if (player.GetComponent<PlayerController>().InteractWithObjects())
                {
                    anim.speed = animSpeed;
                    notUsed = false;
                    StartCoroutine(Wait());
                }
            }
            
        }
    }
    void ActivateObjects()
    {
        anim.speed = stopAnimSpeed;
        transform.GetChild(0).gameObject.SetActive(true); 
    }


    IEnumerator Wait()
    {
       yield return new WaitForSeconds(0.5f);
        objeto.SetActive(true);

    }
}
