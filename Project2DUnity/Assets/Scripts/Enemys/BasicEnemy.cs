﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[RequireComponent(typeof(Perception))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class BasicEnemy : MonoBehaviour {


    //pare dels enemies variables quetinguin tots els enemics aqui
    public int health;
    public int basicDmg;
    public float speed;
    // Use this for initialization

    protected PlayerController player;
    protected Rigidbody2D rb2D;
    protected Animator anim;
    public GameObject keylvl2;
  

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        rb2D.isKinematic = true;
        player = FindObjectOfType<PlayerController>();
       
        anim = GetComponent<Animator>();
    }
    //setters getters 
    public void SetEnemyHp(int newHealth)
    {
        health = newHealth;
    }
    public int GetEnemyHp()
    {
        return health;
    }

    public void SetEnemyDmg(int newDmg)
    {
        basicDmg = newDmg;
    }
    public int GetEnemyDmg()
    {
        return basicDmg;
    }
    protected void CheckHealth()
    {
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }
    protected void CheckHealth(bool boss, int lvl, int hptoDie)
    {
        if(lvl == 1)
        {
            if (health <= hptoDie)
            {

                keylvl2.SetActive(true);
                Destroy(gameObject);
            }
        }
        else
        {
            if (health <= hptoDie)
            {
            
                Destroy(gameObject);
            }
        }
        
    }
}
