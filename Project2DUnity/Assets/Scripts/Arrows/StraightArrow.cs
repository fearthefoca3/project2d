﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StraightArrow : MonoBehaviour
{
    public float speed = 4;
    public int damage = 5;
    public Rigidbody2D rb2D;
    PlayerController player;

    // Use this for initialization
    void Start ()
    {
        rb2D.velocity = transform.up * speed;
        player = GameObject.FindObjectOfType<PlayerController>();

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("HpController"))
        {
            player.SetPlayerHealth(player.GetPlayerHealth() - damage);
        }
        Destroy(gameObject);

    }
}
