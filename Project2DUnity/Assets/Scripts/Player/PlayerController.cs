﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    enum Facing
    {
        Up , Down, Left, Right
    }
    
    [Header("Drag and drop")]//this is to make a title on the inspector
    public Rigidbody2D rb2D;
    public Animator anim;
    public GameObject bulletUp;
    public GameObject bulletDown;
    public GameObject bulletRight;
    public GameObject bulletLeft;
    public Transform shooterUp;
    public Transform shooterDown;
    public Transform shooterRight;
    public Transform shooterLeft;
    [Header("Debug")]//Vars from code
    private float x, y;//joistick x y position.
    Facing pFacing;
    bool IsAttaking;
    public int basicAttackDmg = 10;
    private int dmg;
    public float currentSpeed;
    private const float zeroPos = 0.0f;
    private const int hpToDie = 0;
    public int maxHp = 100;
    public float force = 5.0f;
    public PusherController pusher;
    public bool isPushing;
    public bool death;
    public bool hasKeyLvl2;
    private AudioSource pasicos;
    [SerializeField]private float attakColiderActivationTime = 200.0f;

    [Header("Debug player attributes")]//var from playerthat may afect your gameplay
    [Range(0, 100)] public int hp;
    [Range(0, 5)] public float speed;
    [Range(0, 5)] public float runningSpeed;

    private float attakIncreasedTimer;
    private int dmgInc;

    [Header("Animation Parameteres")]//animation angle 
    [Range(0, 90)] public float angleAnimUp;//angle between UP axe and joystick
    [Range(0, 90)] public float angleAnimDown;//angle between DOWN axe and joystick

    private Transform StoneParticles;

    // Use this for initialization
    void Start()
    {
        pasicos = GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        anim.SetTrigger("Down");
        pFacing = Facing.Down;
        IsAttaking = false;
        hp = 100;
        hasKeyLvl2 = false;
        //  pusher = FindObjectOfType<PusherController>();
        dmg = basicAttackDmg;
        isPushing = false;
        death = false;
        pasicos.Pause();
        StoneParticles = GameObject.Find("StoneParticles").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!death)
        {
            CheckWalkingAnim();
            AttackAnimation(pFacing);
            HpChecker();
           
        }
    }

    void FixedUpdate()
    {
        if (!isPushing && !death)
        {
            MovmentPlayer();

            if (x != 0f || y != 0f)
            {
                pasicos.UnPause();
            }
            else
            {
                pasicos.Pause();
            }
        }

        if (pFacing == Facing.Up)
        {
            StoneParticles.rotation = Quaternion.Euler(0, 0, -90);
        }
        if (pFacing == Facing.Down)
        {
            StoneParticles.rotation = Quaternion.Euler(0, 0, 90);
        }
        if (pFacing == Facing.Left)
        {
            StoneParticles.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (pFacing == Facing.Right)
        {
            StoneParticles.rotation = Quaternion.Euler(0, 0, 180);
        }
    }
    void MovmentPlayer()
    {
        y = Input.GetAxis("Vertical");
        x = Input.GetAxis("Horizontal");
        Vector2 direction = new Vector2(x, y);
        direction.Normalize();
        //with this we destroy the walls
        //Vector2 velocity = new Vector2(x, y) * speed;//now vector amb lavelocitat
        //transform.position += new Vector3(velocity.x * Time.deltaTime, velocity.y * Time.deltaTime, zeroPos);

        rb2D.AddForce(direction * force);
        float clamppedSpeed = Mathf.Clamp(rb2D.velocity.magnitude, -speed, speed);
        rb2D.velocity = direction * clamppedSpeed;

    }

    void CheckWalkingAnim()
    {
        //--------------------------------------------------- animation with controller. Calculates the angle betwen the joistic position and the Y axe,
        //if the angle betwen the joistic vector and Y its bigger than an angle change aniation
        Vector2 vj = new Vector2(x, y);
        vj.Normalize();
        if (x == zeroPos && y == zeroPos && !IsAttaking)
        {
            anim.speed = zeroPos; 
        }
        else
        {
            anim.speed = 1.0f;
            if (y > zeroPos)
            {
                if (angleAnimUp > Vector2.Angle(vj, Vector2.up))
                {
                    ResetAnimation();
                    anim.SetTrigger("Up");
                    pFacing = Facing.Up;
                }
                else if (x > zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Right");
                    pFacing = Facing.Right;
                }
                else if (x < zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Left");
                    pFacing = Facing.Right;
                }
            }
            if (y < zeroPos)
            {
                if (angleAnimDown > Vector2.Angle(vj, Vector2.down))
                {
                    ResetAnimation();
                    anim.SetTrigger("Down");
                    pFacing = Facing.Down;
                }
                else if (x > zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Right");
                    pFacing = Facing.Right;
                }
                else if (x < zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Left");
                    pFacing = Facing.Left;
                }

            }
            if(y == zeroPos)
            {
                if (x > zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Right");
                    pFacing = Facing.Right;
                }
                else if (x < zeroPos)
                {
                    ResetAnimation();
                    anim.SetTrigger("Left");
                    pFacing = Facing.Left;
                }
            }
        }
    }
    void AttackAnimation(Facing face)
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown("joystick button 0"))
        {
            ResetAnimation();
            IsAttaking = true;
            if (face == Facing.Up)
            {
                anim.SetTrigger("Attack_Up");

            }
            else if (face == Facing.Down)
            {
                anim.SetTrigger("Attack_Down");
            }
            else if (face == Facing.Right)
            {
                anim.SetTrigger("Attack_Right");
            }
            else if (face == Facing.Left)
            {
                anim.SetTrigger("Attack_Left");
            }
            StartCoroutine("AttakDamage", face);
        }
        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown("joystick button 1"))
        {
            ResetAnimation();
            IsAttaking = true;
            if (face == Facing.Up)
            {
                anim.SetTrigger("Magic_Up");
            }
            else if (face == Facing.Down)
            {
                anim.SetTrigger("Magic_Down");
            }
            else if (face == Facing.Right)
            {
                anim.SetTrigger("Magic_Right");
            }
            else if (face == Facing.Left)
            {
                anim.SetTrigger("Magic_Left");
            }
        }
    }
    public void ShootMagicUp()
    {
        Instantiate(bulletUp, shooterUp.position, Quaternion.identity);
    }
    public void ShootMagicDown()
    {
        Instantiate(bulletDown, shooterDown.position, Quaternion.identity);
    }
    public void ShootMagicRight()
    {
        Instantiate(bulletRight, shooterRight.position, Quaternion.identity);
    }
    public void ShootMagicLeft()
    {
        Instantiate(bulletLeft, shooterLeft.position, Quaternion.identity);
    }
    public bool InteractWithObjects()
    {
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown("joystick button 3"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool MoveObjects()
    {
        if (Input.GetKey(KeyCode.C) || Input.GetKey("joystick button 2"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator AttakDamage(Facing face)
    {
        transform.GetChild((int)face).gameObject.SetActive(true);
        yield return new WaitForSeconds(attakColiderActivationTime);
        transform.GetChild((int)face).gameObject.SetActive(false);
    }

    void HpChecker()
    {
        if(hp <= hpToDie)
        {
            anim.speed = 1;
            //not implemented yet. The animation ready to use tho
           
            anim.SetTrigger("Death");

            death = true;
                
        }
    }
    //getters setters
    public float GetPlayerSpeed()
    {
        return speed;
    }
    public void SetPlayerSpeed(float newSpeed)
    {
        speed = newSpeed;
    }
    public int GetPlayerHealth()
    {
        return hp;
    }
    public void SetPlayerHealth(int newHp)
    {
        hp = newHp;
    }
    public int GetPlayerMaxHealth()
    {
        return maxHp;
    }
    public void SetPlayerMaxHealth(int newMaxHp)
    {
        maxHp = newMaxHp;
    }
    public int GetPlayerAttackDmg()
    {
        return basicAttackDmg;
    }
    public void SetHasKeyLvl2(bool keyStat)
    {
        hasKeyLvl2 = keyStat;
    }
    public bool GetHasKeyLvl2()
    {
        return hasKeyLvl2;
    }
    public void SetPlayerAttackDmg(int newDmg)
    {
        basicAttackDmg = newDmg;
    }
    public void EndOfAnimation()
    {
        IsAttaking = false;
        ResetAnimation();
    }

    void ResetAnimation()
    {
        anim.ResetTrigger("Up");
        anim.ResetTrigger("Down");
        anim.ResetTrigger("Right");
        anim.ResetTrigger("Left");

        anim.ResetTrigger("Attack_Up");
        anim.ResetTrigger("Attack_Down");
        anim.ResetTrigger("Attack_Right");
        anim.ResetTrigger("Attack_Left");
    }
    public void OnCollisionEnter2D(Collision2D other)
    {

        if (other.collider.CompareTag("Wall") && pusher != null)
        {
            pusher.pushing = false;
            pusher = null;
            isPushing = false;
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy")){//remember to put the tag on the enemy
            
        }
    }
    public void IncreaseAttack(float timerAttackIncreased, int dmgIncrease)
    {
        attakIncreasedTimer = timerAttackIncreased;
        dmgInc = dmgIncrease;
        StartCoroutine("IncreaseTemporaryAttack");
    }
    IEnumerator IncreaseTemporaryAttack()
    {
        SetPlayerAttackDmg(GetPlayerAttackDmg()+dmgInc);
        yield return new WaitForSeconds(attakIncreasedTimer);
        SetPlayerAttackDmg(dmg);

    }
    public void Destroyer()
    {
        StartCoroutine(deathTimer());
    }
    IEnumerator deathTimer()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");

    }
}
