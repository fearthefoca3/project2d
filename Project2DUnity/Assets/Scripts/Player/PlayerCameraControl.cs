﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraControl : MonoBehaviour {

    [Header("Vars to debug and cotrol")]
    public float smoothSpeed = 0.125f;
    [Header("Vars to define")]


    [Header("Vars to drag")]
    public Camera mainCamera;
    public Transform target;

    // Use this for initialization
    void Start () {
        target = FindObjectOfType<PlayerController>().transform;
    }
 
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Corridor"))
        {
            mainCamera.GetComponent<CameraManagement>().cameraChasing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Corridor"))
        {
            mainCamera.GetComponent<CameraManagement>().cameraChasing = false;

        }
    }

}
