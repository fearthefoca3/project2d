﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class bullet : MonoBehaviour
{

    public float speed = 4f;
    public int bulletDmg = 5;
    PlayerController player;
    Vector2 moveDirection;
    //public GameObject explosion;


    private Rigidbody2D rb2D;

    // Use this for initialization
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        player = GameObject.FindObjectOfType<PlayerController>();
        moveDirection = (player.transform.position - transform.position).normalized * speed;
        rb2D.velocity = new Vector2(moveDirection.x, moveDirection.y);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("HpController"))
        {
            player.SetPlayerHealth(player.GetPlayerHealth() - bulletDmg);
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

  
}