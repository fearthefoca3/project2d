﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPlayerMagagment : MonoBehaviour {

    Animator anim;
    PlayerController player;
    public Text txtDamage;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        OnPressU();
        TextInfo();
    }

    void OnPressU()
    {
        if (Input.GetKeyDown(KeyCode.U)/* || Input.GetKeyDown("joystick button 0")*/)
        {
            anim.SetTrigger("GoUp");
        }

        if (Input.GetKeyUp(KeyCode.U))
        {
            anim.SetTrigger("GoDown");
        }
    }

    void TextInfo()
    {

        txtDamage.text = string.Format("Attack Damage:" + player.GetPlayerAttackDmg());
    }
}
