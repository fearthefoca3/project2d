﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManagment : MonoBehaviour {

    [SerializeField] PlayerController player;    // Reference to the player.
    public GameObject enemy;                // The enemy prefab to be spawned.
    public float spawnTime, spawnFirstTime ;            // How long between each spawn.
    public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
    public int up;
    [SerializeField] int spawnPointIndex;
    void Start()
    {
        // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        player = FindObjectOfType<PlayerController>();
        InvokeRepeating("Spawn", spawnFirstTime, spawnTime);
       
    }


    void Spawn()
    {
        // If the player has no health left...
        if (player.death)
        {
            // ... exit the function.
            return;
        }

        // Find a random index between zero and one less than the number of spawn points.
        if (up == 0)
        {
            return;
        }
        else if (up == 1)
        {
            spawnPointIndex = Random.Range(0, 4);

        }
        else if( up== 2)
        {
            spawnPointIndex = Random.Range(4, spawnPoints.Length);

        }

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
