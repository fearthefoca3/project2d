﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BossController : BasicEnemy {


    public int maxHealth;
    [HideInInspector] public int face;
    private Transform target;
    public GameObject bullet;
    public Transform shooter;
    [SerializeField] protected bool attaking;

    [Header("Enemy ratio")]
    public float DistancePlayerBoss = 2.0f;
    public float shootingRatio = 0.8f;
    public bool BossVisible;

    [Header("Canvas")]
    public Image healthBar;

    // Use this for initialization
    void Start () {
        health = maxHealth;
        target = player.GetComponent<Transform>();
        attaking = false;
        shooter =  transform.Find("ShooterBoss");
    }

    private void Update()
    {
        healthBar.fillAmount = (float)health / (float)maxHealth;
    }
    // Update is called once per frame
    private void FixedUpdate()
    {
        if (BossVisible)
        {
            Vector3 direction = target.position - transform.position;
            direction.Normalize();
            if (Vector2.Distance(transform.position, target.position) >= DistancePlayerBoss)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")){
            InvokeRepeating("Shoot", shootingRatio, shootingRatio);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            CancelInvoke();
        }

    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("PlayerBullet"))
        {
            SetEnemyHp(GetEnemyHp() - player.GetPlayerAttackDmg());
            
        }
        if (other.collider.CompareTag("DamageArea"))
        {
            SetEnemyHp(GetEnemyHp() - player.GetPlayerAttackDmg());
        }
        CheckHealth();
    }
    void Shoot()
    {
        Instantiate(bullet, shooter.position, Quaternion.identity);
        
    }
    private void OnBecameInvisible()
    {
        BossVisible = false;
    }
    private void OnBecameVisible()
    {
        BossVisible = true;
    }


}
