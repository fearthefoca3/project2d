﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl1BossActivator : MonoBehaviour {

    public LevelOneBossController enemy;
    public int shootingRatio;
    public Transform spawner;
    public SpawnManagment spM;
    public DoorsController dc;

    // Use this for initialization
    void Start () {
        enemy = FindObjectOfType<LevelOneBossController>();
        spM = FindObjectOfType<SpawnManagment>();
        dc = FindObjectOfType<DoorsController>();
    }

    // Update is called once per frame
    void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            enemy.Active();
            spM.up = 0;
            InvokeRepeating("Shoot", shootingRatio, shootingRatio);
            dc.CloseDoor();
            dc.inside = false;


        }
    }
}
