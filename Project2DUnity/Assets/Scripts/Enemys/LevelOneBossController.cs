﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelOneBossController : BasicEnemy {

    public Transform p1, p2, currentP;
    private bool active = false;
    [Header("Drag")]
    public Transform shooter;
    public GameObject bullet;
    public GameObject BombBoss;
    public GameObject ActivatorBoss;
    [Header("Debug and control")]
    public int hpTransform;
    public int maxHealth;
    public int hpToDie;
    public int currentLvl;
    public float BoombsInstantiateTime = 0.8f;
    public bool activateBomb = false;
    public bool activateShoot = false;
    public float shootingRatio = 1.2f;
    [Header("Canvas")]
    public Image healthBar;

    // Use this for initialization
    void Start () {
        currentP = p1;
    }
	
	// Update is called once per frame
	void Update () {

        if (!active)
            return;

        transform.position = Vector3.MoveTowards(transform.position, currentP.position, speed * Time.deltaTime);
        float dist = (transform.position - currentP.position).sqrMagnitude;
        if (dist < Mathf.Epsilon)
        {
            currentP = (currentP == p1 ? p2 : p1);
        }
        Transformation();
        if (!activateShoot)
        {

            InvokeRepeating("Shoot", shootingRatio, shootingRatio);
            activateShoot = true;
        }
        healthBar.fillAmount = (float)health / (float)maxHealth;
    }
    void Transformation()
    {
        if(health <= hpTransform && health > 0)
        {
            if (!activateBomb)
            {

                InvokeRepeating("BombsOn", BoombsInstantiateTime, BoombsInstantiateTime);

                activateBomb = true;
            }
        }
    }
  

    public void Active()
    {
        active = true;
    }

    public void Shoot()
    {
        Instantiate(bullet, shooter.position, Quaternion.identity);

    }
    public void BombsOn()
    {
        Instantiate(BombBoss, shooter.position, Quaternion.identity);
    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("PlayerBullet"))
        {
            SetEnemyHp(GetEnemyHp() - player.GetPlayerAttackDmg());
        }
        if (other.collider.CompareTag("DamageArea"))
        {
            SetEnemyHp(GetEnemyHp() - player.GetPlayerAttackDmg());
             
        }
        
        CheckHealth(true, currentLvl, hpToDie);
    }
}
