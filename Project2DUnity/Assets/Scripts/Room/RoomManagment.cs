﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RoomManagment : MonoBehaviour {

    [Header("Vars to debug and control")]
    [SerializeField] CameraManagement mainCamera;
    
    public int currentRoom;
    public bool isACorridor;
    public bool needZoom;
    [SerializeField] int lvl;
    [SerializeField] SpawnManagment spawncontroll;
    PlayerController player;

    private void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }
    // Use this for initialization
    void Awake () {
        mainCamera = FindObjectOfType<CameraManagement>();
        spawncontroll = FindObjectOfType<SpawnManagment>();
        lvl = 0;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            if (isACorridor)
            {
                mainCamera.GetComponent<CameraManagement>().cameraChasing=true;
                mainCamera.GetComponent<CameraManagement>().needZoom = false;
            }
         
            else
            {
                mainCamera.GetComponent<CameraManagement>().cameraChasing = false;
                mainCamera.GetComponent<CameraManagement>().roomsChild = currentRoom;
                if (needZoom)
                {
                    mainCamera.GetComponent<CameraManagement>().needZoom = true;
                }  
            }
            if (lvl == 0)
            {
                if (currentRoom == 1)
                    spawncontroll.up = 1;
                else if (currentRoom == 0 || currentRoom == 6 || currentRoom == 7)
                    spawncontroll.up = 0;
                else if (currentRoom == 4)
                    spawncontroll.up = 2;
            }
            else if (lvl == 1)
            {
                if (currentRoom == 1)
                    spawncontroll.up = 1;
                else if (currentRoom == 0)
                    spawncontroll.up = 0;
                else if (currentRoom == 8)
                    spawncontroll.up = 2;
            }
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {

            if (CompareTag("LevelChanger") && other.CompareTag("Player"))
            {

            // implement int to load the lvl depending of the int
            lvl++;

                     if (player.GetHasKeyLvl2())
                     {
                SceneManager.LoadScene("Level2");
                     }
            
            
            }
        
    }
}
