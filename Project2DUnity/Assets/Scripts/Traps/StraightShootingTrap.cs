﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightShootingTrap : MonoBehaviour
{
    public GameObject arrow;
    public Transform firePoint;
    public float fireRate = 10f;

    void Start()
    {
        InvokeRepeating("Shoot", 0.2f, fireRate);
    }

    // Update is called once per frame
    void Update ()
    {
	}
    
    private void Shoot()
    {
        Instantiate(arrow, firePoint.position, firePoint.rotation);
    }
}
