﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagement : MonoBehaviour
{
    [Header("Vars to debug and cotrol")]

    Camera myCamera;

    [Range(0, 1)] public float smoothOfLerp = 0.1f;
    public bool cameraChasing;
    private float cameraZoomDifference;
    private float cameraZoomSpeed;
    private float originalZoom;
    public bool needZoom;
    public int roomsChild;
    private float zoom;
    private int ScreenSizeX = 0;
    private int ScreenSizeY = 0;

    [Header("Vars to define")]


    [Header("Vars to drag")]
    public Transform target;
    public Transform[] Rooms;
    
    // Use this for initialization
    void Start()
    {
        
        myCamera = transform.GetComponent<Camera>();
        cameraChasing = false;
        roomsChild = 0;
        originalZoom= myCamera.orthographicSize;
        RescaleCamera();
        Screen.SetResolution(1920, 1080, true);

    }

    // Update is called once per frame
    void Update ()
    {
        RescaleCamera();
        if (cameraChasing)
        {
            target = FindObjectOfType<PlayerController>().transform;
        }
        else if (!cameraChasing)
        {
            target = FindObjectOfType<Camera>().GetComponent<CameraManagement>().Rooms[roomsChild];
        }

        transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y, transform.position.z), smoothOfLerp);
        zoom = Rooms[roomsChild].GetComponent<Zoom>().roomZoom;

        if (needZoom)
        {
            cameraZoomDifference = zoom - myCamera.orthographicSize;
            cameraZoomSpeed = 1.0f;
            myCamera.orthographicSize += cameraZoomDifference * cameraZoomSpeed * Time.deltaTime;
        }
        else if (!needZoom)
        {
            cameraZoomDifference = originalZoom -myCamera.orthographicSize;
            cameraZoomSpeed = 1.0f;
            myCamera.orthographicSize += cameraZoomDifference * cameraZoomSpeed * Time.deltaTime;
        }
    }



    private void RescaleCamera()
    {

        if (Screen.width == ScreenSizeX && Screen.height == ScreenSizeY) return;

        //forzar el aspect ratio a 16:9
        float targetaspect = 16.0f / 9.0f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Camera camera = GetComponent<Camera>();

        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            camera.rect = rect;
        }
        else 
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }

        ScreenSizeX = Screen.width;
        ScreenSizeY = Screen.height;
    }

    void OnPreCull()
    {
        if (Application.isEditor) return;
        Rect wp = Camera.main.rect;
        Rect nr = new Rect(0, 0, 1, 1);

        Camera.main.rect = nr;
        GL.Clear(true, true, Color.black);

        Camera.main.rect = wp;

    }

}
